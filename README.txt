
Module: Tracking Inject
Author: Peter Sawczynec <https://www.drupal.org/u/drupaldeep>


Description
===========
Adds the ability for injecting tracking scripts into website pages.

Tracking scripts are typically provided by an advertising agency, research firm,
analytics firm, or other provider to track the page views or other user activity
on your site.

Tracking scripts often need to be targeted to one web page, some web pages or
all web pages on a website and tracking scripts are typically used to track
email campaign response, special event advertising and marketing campaigns or
split-run web page content testing.

The Tracking Inject module gives you the ability to target and position tracking
script injections from a centralized manageable admin interface:
- Target on which page or page(s) each tracking script is to be included. You
  can target by by entity type(s), taxonomy term(s), page path(s) and home page.
- Position where within the page HTML each tracking script should appear.
  Default positions include HEAD, after opening body tag and before
  closing body tag. These are the page positions that are most commonly required
  for deploying tracking scripts on a site.
- There is also a blanket 'sitewide-tracking' which will inject the tracking
  script on every page.

Requirements
============

* To administer the Tracking Inject module, the user must have the
  'Manage Tracking Injections' permission


Installation
============
Copy the 'tracking_inject' module directory to your Drupal 'modules'
directory as usual.


Usage
=====
On the Tracking Injections settings page enter the top-level
domain name of your site, e.g.: examplesite.com

Add the tracking injections you need on your site.

Targeted and positioned tracking
====================================================
Visit the Tracking Inject administration pages and follow the inline help text.
