<?php

/**
 * @file
 * Contains \Drupal\tracking_inject\Form\TrackingInjectAdmin.
 */

namespace Drupal\tracking_inject\Form;

use Drupal\Core\Form\FormBase;
use Drupal\tracking_inject\TrackingInjectManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Displays tracking injections.
 */
class TrackingInjectAdmin extends FormBase {

  /**
   * @var \Drupal\tracking_inject\TrackingInjectManagerInterface
   */
  protected $trackingInjectManager;

  /**
   * Constructs a new TrackingInjectAdmin object.
   *
   * @param \Drupal\tracking_inject\TrackingInjectManagerInterface $tracking_inject_manager
   */
  public function __construct(TrackingInjectManagerInterface $tracking_inject_manager) {
    $this->trackingInjectManager = $tracking_inject_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tracking_inject.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tracking_inject_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tracking_inject.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param string $default_ip
   *   (optional) IP address to be passed on to
   *   \Drupal::formBuilder()->getForm() for use as the default value of the IP
   *   address form field.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $default_ip = '') {

    $config = $this->config('tracking_inject.settings');

    $build['tracking_inject_settings_form']  = \Drupal::formBuilder()->getForm('Drupal\tracking_inject\Form\TrackingInjectSettings');
    $rows = array();
    $header = array($this->t('Tracking injections for this site'), $this->t('Operations'));
    $result = $this->trackingInjectManager->findAll();
    foreach ($result as $ti) {
      $row = array();
      $row[] = $ti->title;
      $links = array();
      $links['edit'] = array(
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('tracking_inject.edit', ['tracking_inject_id' => $ti->id]),
      );
      $links['delete'] = array(
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('tracking_inject.delete', ['tracking_inject_id' => $ti->id]),
      );
      $row[] = array(
        'data' => array(
          '#type' => 'operations',
          '#links' => $links,
        ),
      );
      $rows[] = $row;
    }

    $build['tracking_inject_table'] = array(
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No tracking injections found.'),
      '#weight' => 120,
    );

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ti = trim($form_state->getValue('id'));
    $form_values['id'] = $ti;
    $form_values['domain'] = trim($form_state->getValue('domain'));
    $form_values['title'] = trim($form_state->getValue('title'));
    $form_values['hide_taxonomy_vocab'] = trim($form_state->getValue('hide_taxonomy_vocab'));
    $form_values['hide_content_entity'] = trim($form_state->getValue('hide_content_entity'));
    $form_values['target_pages'] = trim($form_state->getValue('target_pages'));
    $form_values['tracking_info'] = trim($form_state->getValue('tracking_info'));
    $form_values['position'] = trim($form_state->getValue('position'));
    $form_values['weight'] = trim($form_state->getValue('weight'));
    $this->trackingInjectManager->addTrackingInjection($form_values);
    drupal_set_message($this->t('The tracking injection %ti has been added.', array('%ti' => $ti)));
    $form_state->setRedirect('tracking_inject.admin_page');
  }

}
