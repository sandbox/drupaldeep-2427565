<?php

/**
 * @file
 * Contains \Drupal\tracking_inject\Form\TrackingInjectDelete.
 */

namespace Drupal\tracking_inject\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\tracking_inject\TrackingInjectManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to remove tracking injections.
 */
class TrackingInjectDelete extends ConfirmFormBase {

  /**
   * The tracking injection.
   *
   * @var string
   */
  protected $trackingInjectionID;

  /**
   * Constructs a new TrackingInjectDelete object.
   *
   * @param \Drupal\tracking_inject\TrackingInjectManagerInterface $tracking_inject_manager
   *   The Tracking Inject manager.
   */
  public function __construct(TrackingInjectManagerInterface $tracking_inject_manager) {
    $this->trackingInjectManager = $tracking_inject_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tracking_inject.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tracking_inject_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tracking_inject.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete tracking injection id %ti?', array('%ti' => $this->trackingInjectionID));
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('tracking_inject.admin_page');
  }

  /**
   * {@inheritdoc}
   *
   * @param string $tracking_inject_id
   *   The tracking inject ID to delete.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $tracking_inject_id = '') {
    $tracking_result = $this->trackingInjectManager->findById($tracking_inject_id);
    $tracking_injection = array_shift($tracking_result);
    $this->trackingInjectionID = $tracking_injection->id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->trackingInjectManager->removeTrackingInjection($this->trackingInjectionID);
    $this->logger('user')->notice('Deleted %ti', array('%ti' => $this->trackingInjectionID));
    drupal_set_message($this->t('The Tracking injection %ti was deleted.', array('%ti' => $this->trackingInjectionID)));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
