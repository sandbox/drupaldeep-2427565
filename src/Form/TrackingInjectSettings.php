<?php

/**
 * @file
 * Contains \Drupal\tracking_inject\Form\TrackingInjectSettings.
 */

namespace Drupal\tracking_inject\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the tracking inject admin global settings form.
 */
class TrackingInjectSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tracking_inject_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tracking_inject.settings'];
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('tracking_inject.settings');

    $form['#attributes'] = array('class' => array('tracking-inject-settings-form'));
    $form['tracking_inject_settings'] = array(
      '#type' => 'details',
      '#title' => t('Tracking Injection settings'),
      '#open' => TRUE,
    );

    $form['tracking_inject_settings']['domain'] = array(
      '#default_value' => $config->get('domain'),
      '#description' => t('This top-level domain name of your site.'),
      '#maxlength' => 64,
      '#placeholder' => 'example.com',
      '#required' => TRUE,
      '#size' => 15,
      '#title' => t('Top-level domain'),
      '#type' => 'textfield',
    );
    $form['tracking_inject_settings']['submit'] = array(
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('tracking_inject.settings');
    $config
      ->set('domain', $form_state->getValue('domain'))
      ->save();
    $form_state->setRedirect('tracking_inject.admin_page');
  }

  /**
   * Resets the tracking inject global settings selections.
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('tracking_inject.admin_page');
  }

}
