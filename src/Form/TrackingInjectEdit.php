<?php

/**
 * @file
 * Contains \Drupal\tracking_inject\Form\TrackingInjectEdit.
 */

namespace Drupal\tracking_inject\Form;

use Drupal\Core\Form\FormBase;
use Drupal\tracking_inject\TrackingInjectManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Displays tracking injections for editing.
 */
class TrackingInjectEdit extends FormBase {

  /**
   * @var \Drupal\tracking_inject\TrackingInjectManagerInterface
   */
  protected $trackingInjectManager;

  /**
   * Constructs a new TrackingInjectAdmin object.
   *
   * @param \Drupal\tracking_inject\TrackingInjectManagerInterface $tracking_inject_manager
   */
  public function __construct(TrackingInjectManagerInterface $tracking_inject_manager) {
    $this->trackingInjectManager = $tracking_inject_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tracking_inject.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tracking_inject_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tracking_inject.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param string $tracking_inject_id
   *   (optional) $tracking_inject_id to be passed on to
   *   \Drupal::formBuilder()->getForm() for use to get the default values
   *   for all the form fields.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $tracking_inject_id = '') {
    $config = $this->config('tracking_inject.settings');

    $tracking_result = $this->trackingInjectManager->findById($tracking_inject_id);
    $edit_values = (array) array_shift($tracking_result);

    $form['domain'] = array(
      '#default_value' => $edit_values['domain'],
      '#description' => t('Your default top-level domain is @domain. If you need this Tracking Injection to be more targeted for this tracking injection only, change it here.', array('@domain' => $config->get('domain'))),
      '#maxlength' => 20,
      '#placeholder' => 'UA-',
      '#required' => TRUE,
      '#size' => 24,
      '#title' => t('Tracking Injection Domain'),
      '#type' => 'textfield',
    );

    $form['tracking_domain']['title'] = array(
      '#title' => $this->t('Title'),
      '#type' => 'textfield',
      '#size' => 48,
      '#maxlength' => 40,
      '#default_value' => $edit_values['title'],
      '#description' => $this->t('Enter a title for this Tracking Injection.'),
    );
    $form['tax_set'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage Display By Taxonomy'),
      '#description' => '',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $vocab_options = array();
    if (\Drupal::moduleHandler()->moduleExists('taxonomy')) {
      $vocabularies = entity_load_multiple('taxonomy_vocabulary');
      $vocab_options = array();
      foreach ($vocabularies as $voc) {
        $vocab_options[$voc->id()] = $voc->label();
      }
    }
    $form['tax_set']['hide_taxonomy_vocab'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Taxonomy vocabulary pages to include for this tracking'),
      '#default_value' => unserialize($edit_values['hide_taxonomy_vocab']),
      '#options' => $vocab_options,
      '#multiple' => TRUE,
    );
    $form['ent_set'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage Display By Content Types and Entities'),
      '#description' => '',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $node_type_options = array();
    foreach (node_type_get_types() as $key => $val) {
      $node_type_options[$key] = $val->label();
    }
    $form['ent_set']['hide_content_entity'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Content types (entity bundles) to include for this tracking'),
      '#default_value' => unserialize($edit_values['hide_content_entity']),
      '#options' => $node_type_options,
      '#multiple' => TRUE,
    );
    $form['target_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Target pages'),
      '#default_value' => $edit_values['target_pages'],
      '#description' => t('Leave blank if there are no targeted page paths. <br />To target pages, enter a comma separated list of page url aliases. <br />If this tracking item should appear on all pages, enter only "sitewide-tracking".  <br />To target Home page, enter "home". <br />To exclude a page use tilde before the alias e.g.: "~home" or "~spring-summer".'),
      '#required' => FALSE,
      '#size' => '30',
      '#suffix' => '</div>',
    );
    $form['tracking_info'] = array(
      '#type' => 'textarea',
      '#title' => t('Tracking'),
      '#default_value' => $edit_values['tracking_info'],
      '#description' => t('Tracking info to place into page'),
      '#required' => TRUE,
      '#size' => '5',
      '#prefix' => '<div id="mmg_tracking_icao_replace">',
    );
    /*
     * For Drupal 8 by default there are only 3 valid regions for injection:
     * HEAD: <HEAD> tag area
     * page_top: just after opening body tag
     * page_bottom: just before closing body tag.
     */
    $all_regions = $config->get('regions');
    $regions_options = array();
    foreach ($all_regions as $key => $value) {
      $regions_options[$key] = $value;
    }
    $form['position'] = array(
      '#type' => 'select',
      '#title' => t('Tracking output location in page'),
      '#default_value' => $edit_values['position'],
      '#options' => $regions_options,
    );
    $form['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#default_value' => $edit_values['weight'],
      '#description' => t('Optional. In the output, the heavier tracking will output later and the lighter tracking will be output nearer the top.'),
    );
    $form['id'] = array(
      '#type' => 'value',
      '#value' => $edit_values['id'],
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ti = trim($form_state->getValue('id'));
    $form_values['id'] = $ti;
    $form_values['domain'] = trim($form_state->getValue('domain'));
    $form_values['title'] = trim($form_state->getValue('title'));
    $form_values['hide_taxonomy_vocab'] = serialize($form_state->getValue('hide_taxonomy_vocab'));
    $form_values['hide_content_entity'] = serialize($form_state->getValue('hide_content_entity'));
    $form_values['target_pages'] = trim($form_state->getValue('target_pages'));
    $form_values['tracking_info'] = trim($form_state->getValue('tracking_info'));
    $form_values['position'] = $form_state->getValue('position');
    $form_values['weight'] = trim($form_state->getValue('weight'));
    $this->trackingInjectManager->updateTrackingInjection($form_values);
    drupal_set_message($this->t('The tracking injection %ti has been updated.', array('%ti' => trim($form_state->getValue('title')))));
    $form_state->setRedirect('tracking_inject.admin_page');
  }

}
