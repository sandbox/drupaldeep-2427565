<?php

/**
 * @file
 * Contains \Drupal\tracking_inject\EventSubscriber\TrackingInjectEventSubscriber.
 */

namespace Drupal\tracking_inject\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TrackingInjectEventSubscriber implements EventSubscriberInterface {

  /**
   * Stub function.
   */
  public function initiateTracking(GetResponseEvent $event) {
    // Redirect example:
    // print "<br /> init event subscriber tracking REQUEST event<br />";
    // if ($event->getRequest()->query->get('redirect-me')) {
    // $event->setResponse(new RedirectResponse('http://example.com/'));
    // }
  }

  /**
   * {@inheritdoc}
   */
  static public function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('initiateTracking');
    return $events;
  }

}
