<?php

/**
 * @file
 * Contains \Drupal\tracking_inject\TrackingInjectInterface.
 */

namespace Drupal\tracking_inject;

/**
 * Provides an interface defining a TrackingInject
 */
interface TrackingInjectInterface {

  /**
   * {@inheritdoc}
   */
  public function getTrackingInjectDomains();

  /**
   * {@inheritdoc}
   */
  public function setTrackingInjectDomains();
  /**
   * {@inheritdoc}
   */
  public function getTrackingInjectRegions();

  /**
   * {@inheritdoc}
   */
  public function setTrackingInjectRegions();

  /**
   * {@inheritdoc}
   */
  public function getTrackingInjectCollections();

  /**
   * {@inheritdoc}
   */
  public function setTrackingInjectCollections($tracking_collection);

  /**
   * {@inheritdoc}
   */
  public function trackingInjectEnactTracking();

  /**
   * {@inheritdoc}
   */
  public function trackingInjectOutputs($domain, $target_region);


  /**
   * {@inheritdoc}
   */
  public function trackingInjectProcessOutput($tracking_item);
  /**
   * Analyze page path properties to decide if tracking is required.
   *
   * @param array $targeted_taxonomies
   *   Vocabularies tracking injections should be on.
   * @param array $targeted_entities
   *   Entity types tracking injections should be on.
   *
   * @returns string page type.
   */
  public function trackingInjectGetTrackablePgType($targeted_taxonomies, $targeted_entities);

  /**
   * Returns a valid page type.
   *
   * @return string
   *   Valid page type.
   */
  public function trackingInjectGetEntityInfo($valid_entity_type, $targeted_taxonomies, $targeted_entities);

  /**
   * Returns a valid page type.
   *
   * @return string
   *   Valid page type.
   */
  public function trackingInjectIsValidPgType();

  /**
   * Execute the match process.
   */
  public function executeMatch($target_pages);

  /**
   * Match the subject against a set of regex patterns.
   *
   * Similar to drupal_match_path() but also handles negation through the use
   * of the ~ character.
   *
   * @param mixed $subject
   *   The subject string or an array of strings to be matched.
   * @param array $patterns
   *   An array of patterns. Any patterns that begin with ~ are considered
   *   negative or excluded conditions.
   * @param bool $path
   *   Whether the given subject should be matched as a Drupal path. If TRUE,
   *   '<front>' will be replaced with the site frontpage when matching against
   *   $patterns.
   */
  public function matchProcess($subject, $patterns, $path = FALSE);

  /**
   * Evaluate path and page type properties to make display decision.
   *
   * @param bool $page_state
   *   Status of page type display decision.
   * @param bool $path_state
   *   Status of path display decision.
   */
  public function performTrackingDecision($page_state, $path_state);

  /**
   * Get all currently used tracking locations for a domain display.
   *
   * @param string $domain
   *   The domain for which to get tracking injections.
   * @param string $target_region
   *   The target region for which to get tracking injections.
   *
   * @return array
   *   An array of sorted tracking injections.
   */
  public function trackingInjectGetInjectionsInUse($domain, $target_region);

  /**
   * Return tracking settings for a specific ID.
   *
   * If there are no settings yet, get the default settings instead.
   *
   * @param int $tracking_id
   *   A tracking id to get data for.
   *
   * @return array
   *   Tracking injection configuration.
   */
  public function trackingInjectGetInjectionSettings($tracking_id);

  /**
   * Get all currently used domains.
   *
   * @param string $domain
   *   The site default domain.
   *
   * @return array
   *   An array of sorted domains.
   */
  public function trackingInjectGetDomainsInUse($domain);

  /**
   * Check if a date is between a start and end date.
   *
   * @param string $start_date
   *   The start date of date range.
   *
   * @param string $end_date
   *   The end date of date range.
   *
   * @param string $date_to_check
   *   The date to check if it is within range.
   *
   * @return bool
   *   True if date is in range, false if not.
   */
  public function trackingInjectCheckDateInRange($start_date, $end_date, $date_to_check);

}
