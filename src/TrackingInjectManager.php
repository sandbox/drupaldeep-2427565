<?php

/**
 * @file
 * Definition of Drupal\tracking_inject\TrackingInjectManager.
 */

namespace Drupal\tracking_inject;

use Drupal\Core\Database\Connection;

/**
 * Tracking Inject manager.
 */
class TrackingInjectManager implements TrackingInjectManagerInterface {

  /**
   * The database connection used to store tracking injection data.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Construct the TrackingInjectManager.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection which will be used to check the IP against.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function activeTrackingInjection($id) {
    return (bool) $this->connection->query("SELECT * FROM {tracking_inject} WHERE id = :id", array(':id' => $id))->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function findAll() {
    return $this->connection->query('SELECT * FROM {tracking_inject}');
  }

  /**
   * {@inheritdoc}
   */
  public function updateTrackingInjection($form_values) {
    $this->connection->update('tracking_inject')
      ->fields(array(
        'domain' => $form_values['domain'],
        'title' => $form_values['title'],
        'hide_taxonomy_vocab' => $form_values['hide_taxonomy_vocab'],
        'hide_content_entity' => $form_values['hide_content_entity'],
        'target_pages' => $form_values['target_pages'],
        'tracking_info' => $form_values['tracking_info'],
        'position' => $form_values['position'],
        'weight' => $form_values['weight'],
      ))
      ->condition('id', $form_values['id'], '=')
      ->execute();
  }
  /**
   * {@inheritdoc}
   */
  public function addTrackingInjection($form_values) {
    $this->connection->insert('tracking_inject')
      ->fields(array(
         'domain' => $form_values['domain'],
         'title' => $form_values['title'],
         'hide_taxonomy_vocab' => $form_values['hide_taxonomy_vocab'],
         'hide_content_entity' => $form_values['hide_content_entity'],
         'target_pages' => $form_values['target_pages'],
         'tracking_info' => $form_values['tracking_info'],
         'position' => $form_values['position'],
         'weight' => $form_values['weight'],
      ))
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function removeTrackingInjection($id) {
    $this->connection->delete('tracking_inject')
      ->condition('id', $id)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function findById($id) {
    return $this->connection->query("SELECT * FROM {tracking_inject} WHERE id = :id", array(':id' => $id))->fetchAll();
  }

  /**
   * Common elements of the tracking inject add and edit forms.
   */
  public function commonForm(array &$form) {
    /* @var $language \Drupal\language\ConfigurableLanguageInterface */
    $language = $this->entity;
    if ($language->getId()) {
      $form['langcode_view'] = array(
        '#type' => 'item',
        '#title' => $this->t('Language code'),
        '#markup' => $language->id(),
      );
      $form['langcode'] = array(
        '#type' => 'value',
        '#value' => $language->id(),
      );
    }
    else {
      $form['langcode'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Language code'),
        '#maxlength' => 12,
        '#required' => TRUE,
        '#default_value' => '',
        '#disabled' => FALSE,
        '#description' => $this->t('Use language codes as <a href="@w3ctags">defined by the W3C</a> for interoperability. <em>Examples: "en", "en-gb" and "zh-hant".</em>', array('@w3ctags' => 'http://www.w3.org/International/articles/language-tags/')),
      );
    }
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Language name in English'),
      '#maxlength' => 64,
      '#default_value' => $language->label(),
      '#required' => TRUE,
    );
    $form['direction'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Direction'),
      '#required' => TRUE,
      '#description' => $this->t('Direction that text in this language is presented.'),
      '#default_value' => $language->getDirection(),
      '#options' => array(
        LanguageInterface::DIRECTION_LTR => $this->t('Left to right'),
        LanguageInterface::DIRECTION_RTL => $this->t('Right to left'),
      ),
    );

    return $form;
  }

}
