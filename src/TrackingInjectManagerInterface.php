<?php

/**
 * @file
 * Contains \Drupal\tracking_inject\TrackingInjectManagerInterface.
 */

namespace Drupal\tracking_inject;

/**
 * Provides an interface defining a TrackingInject manager.
 */
interface TrackingInjectManagerInterface {

  /**
   * Returns if this tracking injection is found.
   *
   * @param string $id
   *   The tracking injection to find.
   *
   * @return bool
   *   TRUE if the tracking injection is found, FALSE otherwise.
   */
  public function activeTrackingInjection($id);

  /**
   * Finds all tracking injections.
   *
   * @return \Drupal\Core\Database\StatementInterface
   *   The result of the database query.
   */
  public function findAll();

  /**
   * Saves a tracking injection item.
   *
   * @param array $form_values
   *   The tracking injection item values.
   */
  public function addTrackingInjection($form_values);

  /**
   * Updates a tracking injection item.
   *
   * @param array $form_values
   *   The tracking injection item values.
   */
  public function updateTrackingInjection($form_values);

  /**
   * Deletes a tracking injection item.
   *
   * @param int $id
   *   The tracking injection to delete.
   */
  public function removeTrackingInjection($id);

  /**
   * Finds a tracking injection by its ID.
   *
   * @param int $id
   *   The ID for a tracking injection.
   *
   * @return string|false
   *   Either the tracking injection data or FALSE if none exist with that ID.
   */
  public function findById($id);

}
