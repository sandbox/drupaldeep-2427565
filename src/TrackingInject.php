<?php

/**
 * @file
 * Contains \Drupal\tracking_inject\TrackingInject.
 */

namespace Drupal\tracking_inject;

use Drupal\Core\Config\ConfigFactory;
use Drupal\tracking_inject\TrackingInjectManagerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides tracking injection to implement Tracking Injections.
 */
class TrackingInject implements TrackingInjectInterface{

  /**
   * The page request to evaluate for tracking injections.
   *
   * @var \Drupal\tracking_inject\TrackingInject
   */
  private $trackingInjectRequest;

  /**
   * The tracking injection target domains.
   *
   * @var \Drupal\tracking_inject\TrackingInject
   */
  private $trackingInjectDomains;

  /**
   * The tracking injection target regions.
   *
   * @var \Drupal\tracking_inject\TrackingInject
   */
  private $trackingInjectRegions;

  /**
   * The tracking injection collections.
   *
   * @var \Drupal\tracking_inject\TrackingInject
   */
  private $trackingInjectCollections;

  /**
   * @var \Drupal\tracking_inject\TrackingInjectManagerInterface
   */
  protected $trackingInjectManager;

  /**
   * Constructs a TrackingInject object.
   *
   * @param array $tracking_target_region
   *   The page region into which tracking items get injected.
   */
  public function __construct() {
    if (!is_array($this->getTrackingInjectCollections())) {
      $this->trackingInjectRequest = \Drupal::request();
      $this->setTrackingInjectDomains();
      $this->setTrackingInjectRegions();
      $this->trackingInjectEnactTracking();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackingInjectDomains() {
    return $this->trackingInjectDomains;
  }

  /**
   * {@inheritdoc}
   */
  public function setTrackingInjectDomains() {
    $target_domains = \Drupal::config('tracking_inject.settings')
      ->get('domain');
    $this->trackingInjectDomains = $target_domains;
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackingInjectRegions() {
    return $this->trackingInjectRegions;
  }

  /**
   * {@inheritdoc}
   */
  public function setTrackingInjectRegions() {
    $target_regions = \Drupal::config('tracking_inject.settings')
      ->get('regions');
    $this->trackingInjectRegions = $target_regions;
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackingInjectCollections() {
    return $this->trackingInjectCollections;
  }

  /**
   * {@inheritdoc}
   */
  public function setTrackingInjectCollections($tracking_collection) {
    $this->trackingInjectCollections = $tracking_collection;
  }

  /**
   * {@inheritdoc}
   */
  public function trackingInjectEnactTracking() {
    $domains = $this->getTrackingInjectDomains();
    $default_regions = $this->getTrackingInjectRegions();
    $route = $this->trackingInjectRequest->getRequestUri();
    $tracking_items = array();

    // If is Admin, exit tracking.
    // Should be adminable and more inclusive.
    if (strstr($route, 'admin') || strstr($route, 'node/add')) {
      return $tracking_items;
    }

    // Get page http status code for additional tracking inject filtering.
    $status = NULL;
    if ($exception = $this->trackingInjectRequest->attributes->get('exception')) {
      $status = $exception->getStatusCode();
    }
    // 403 Forbidden, 404 Not Found.
    $trackable_status_codes = array(
      '403',
      '404',
    );

    $domains_in_use = (is_array($domains)) ? $domains : array($domains);

    if (!empty($domains_in_use)) {
      foreach ($domains_in_use as $domain_in_use) {
        if ($default_regions) {
          foreach ($default_regions as $key => $val) {
            $tracking_region = $key;
            // Region tracking potential outputs.
            $tracking_injects = $this->trackingInjectOutputs($domain_in_use, $tracking_region);
            if ($tracking_injects) {
              asort($tracking_injects['tracking_' . $tracking_region . '_output']);
              foreach ($tracking_injects['tracking_' . $tracking_region . '_output'] as $tracking_item) {
                // Add this tracking to region?
                $show_decision = $this->trackingInjectProcessOutput($tracking_item);
                if ($show_decision) {
                  $tracking_items[$tracking_region][] = $tracking_item;
                }

              }
            }

          }
        }
      }
    }

    $this->setTrackingInjectCollections($tracking_items);

    return $tracking_items;
  }

  /**
   * {@inheritdoc}
   */
  public function trackingInjectOutputs($domain, $target_region) {
    $tracking_injects = array();

    $tracking_instances = $this->trackingInjectGetInjectionsInUse($domain, $target_region);

    if ($tracking_instances) {
      foreach ($tracking_instances as $tracking_instance) {
        $tracking_title = $tracking_instance->title;
        $tracking_output = $tracking_instance->tracking_info;
        $targeted_pages = $tracking_instance->target_pages;
        $targeted_taxonomies = unserialize($tracking_instance->hide_taxonomy_vocab);
        $targeted_entities = unserialize($tracking_instance->hide_content_entity);

        ${'tracking_' . $target_region}[] = array(
          $tracking_output,
          $targeted_pages,
          $targeted_taxonomies,
          $targeted_entities,
          $tracking_title,
        );
        $tracking_injects['tracking_' . $target_region . '_output'] = ${'tracking_' . $target_region};
      }
    }

    return $tracking_injects;
  }

  /**
   * {@inheritdoc}
   */
  public function trackingInjectProcessOutput($tracking_item) {
    $is_trackable_pg_type = FALSE;
    $is_trackable_path = FALSE;
    $show_decision = FALSE;

    // Check if this page is of a type to track as set in the Admin.
    $is_trackable_pg_type = $this->trackingInjectGetTrackablePgType($tracking_item[2], $tracking_item[3]);
    $target_paths = explode(',', str_replace(' ', '', $tracking_item[1]));
    $is_trackable_path = $this->executeMatch($target_paths);
    $show_decision = $this->performTrackingDecision($is_trackable_pg_type, $is_trackable_path);

    return $show_decision;
  }

  /**
   * {@inheritdoc}
   */
  public function trackingInjectGetTrackablePgType($targeted_taxonomies, $targeted_entities) {
    $evaluation_result = 0;
    // Check first if is either taxonomy or node.
    $essential_pg_type = $this->trackingInjectIsValidPgType();
    if ($essential_pg_type == "node" || $essential_pg_type == "taxonomy_term") {
      $evaluation_result = $this->trackingInjectGetEntityInfo($essential_pg_type, $targeted_taxonomies, $targeted_entities);
    }
    return $evaluation_result;
  }

  /**
   * {@inheritdoc}
   */
  public function trackingInjectGetEntityInfo($valid_entity_type, $targeted_taxonomies, $targeted_entities) {
    $compare_array = array();
    $is_valid_type = 0;
    $get_id = 0;
    $path = drupal_get_destination();

    if ($valid_entity_type == "taxonomy_term") {
      $get_id = explode('/', $path['destination']);
      $get_id = $get_id[2];
      $compare_array = $targeted_taxonomies;
    }
    if ($valid_entity_type == "node") {
      $get_id = explode('/', $path['destination']);
      $get_id = $get_id[1];
      $compare_array = $targeted_entities;
    }
    $e_id = (int) $get_id;
    $o_entity = entity_load($valid_entity_type, $e_id);
    // Get this entity parent type.
    $entity_parent_type = $o_entity->bundle();

    // Check if this tracking instance has any taxonomy or
    // node/entity settings in tracking Admin.
    if (is_array($compare_array)) {
      // Compare current entity parent type to allowed types in tracking Admin.
      $is_valid_type = (array_intersect($compare_array, array($entity_parent_type))) ? 1 : 0;
    }

    return $is_valid_type;
  }

  /**
   * {@inheritdoc}
   */
  public function trackingInjectIsValidPgType() {

    $pg_path = drupal_get_destination();

    // Analyze for valid page type only if not Home Page,
    // Home Page is found not by page type but by path.
    if ($pg_path['destination'] == \Drupal::config('system.site')
        ->get('page.front')
    ) {
      // This is the Home Page, valid page type check is not needed.
      return FALSE;
    }

    $valid_type = (strstr($pg_path['destination'], "taxonomy") ? "taxonomy_term" : FALSE);
    $valid_type = (strstr($pg_path['destination'], "node") ? "node" : $valid_type);

    return $valid_type;
  }

  /**
   * {@inheritdoc}
   */
  public function executeMatch($target_pages) {
    $is_sitewide_tracking = FALSE;

    // Include both the path alias and normal path for matching.
    // home page is detected as 'home', no as '/'
    $path_from_alias = $this->trackingInjectRequest->getRequestUri();
    $path_from_query = $this->trackingInjectRequest->attributes->get('_system_path');
    $current_path = array($path_from_alias == '/' ? $path_from_alias : ltrim($path_from_alias, '/'));
    if ($current_path != $path_from_query) {
      $current_path[] = $path_from_query;
    }

    $paths = $target_pages;

    if (!array_filter($paths)) {
      // No paths to evaluate, exit with no match process.
      return 'NOEVAL';
    }

    if (in_array('sitewide-tracking', $paths)) {
      $is_sitewide_tracking = TRUE;
    }

    $match_results = $this->matchProcess($current_path, $paths, TRUE);

    if ($is_sitewide_tracking || $match_results['state']) {
      $negated_path = '~' . $current_path[0];
      if ($match_results['type'] == "negated" && in_array($negated_path, $paths)) {
        return FALSE;
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function matchProcess($subject, $patterns, $path = FALSE) {
    static $regexps;
    $front_pg = \Drupal::config('system.site')->get('page.front');

    $match['type'] = '';
    $match['state'] = FALSE;
    $positives = $negatives = 0;
    $subject = !is_array($subject) ? array($subject) : $subject;
    foreach ($patterns as $pattern) {
      if (strpos($pattern, '~') === 0) {
        $match['type'] = 'negated';
        $negate = TRUE;
        $negatives++;
      }
      else {
        $negate = FALSE;
        $positives++;
      }
      $pattern = ltrim($pattern, '~');
      if (!isset($regexps[$pattern])) {
        if ($path) {
          $regexps[$pattern] = '/^(' . preg_replace(array(
              '/(\r\n?|\n)/',
              '/\\\\\*/',
              '/(^|\|)\\\\<front\\\\>($|\|)/'
            ), array(
              '|',
              '.*',
              '\1' . preg_quote($front_pg, '/') . '\2'
            ), preg_quote($pattern, '/')) . ')$/';
        }
        else {
          $regexps[$pattern] = '/^(' . preg_replace(array(
              '/(\r\n?|\n)/',
              '/\\\\\*/'
            ), array('|', '.*'), preg_quote($pattern, '/')) . ')$/';
        }
      }
      foreach ($subject as $value) {
        if (preg_match($regexps[$pattern], $value)) {
          if ($negate) {
            $match['state'] = FALSE;
          }
          $match['state'] = TRUE;
        }
      }
    }
    // If there are **only** negative conditions and we've gotten this far none
    // we actually have a match.
    if ($positives === 0 && $negatives) {
      $match['state'] = TRUE;
    }

    return $match;
  }

  /**
   * {@inheritdoc}
   */
  public function performTrackingDecision($page_state, $path_state) {

    // Decision by path always wins.
    if ($path_state === TRUE) {
      return TRUE;
    }
    // If no path set in Admin, only page state matters in evaluation.
    if ($path_state !== TRUE && $path_state == 'NOEVAL') {
      if ($page_state == TRUE) {
        return TRUE;
      }
      if ($page_state == FALSE) {
        return FALSE;
      }
    }
    if ($page_state == TRUE && $path_state == TRUE) {
      return TRUE;
    }
    if ($page_state == TRUE && $path_state == FALSE) {
      return FALSE;
    }
    if ($page_state == FALSE && $path_state == TRUE) {
      return TRUE;
    }
    if ($page_state == FALSE && $path_state == FALSE) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function trackingInjectGetInjectionsInUse($domain, $target_region) {
    return db_query('SELECT * FROM {tracking_inject}
    WHERE domain=:domain AND position=:target_region ORDER BY weight ASC',
      array(':domain' => $domain, ':target_region' => $target_region));
  }

  /**
   * {@inheritdoc}
   */
  public function trackingInjectGetInjectionSettings($tracking_id) {
    $settings = db_query('SELECT * FROM {tracking_inject} WHERE id=:id',
      array(':id' => $tracking_id))->fetchObject();
    if (empty($settings)) {
      // There are no settings. Get module's default settings.
      $settings = new stdClass();
      $settings->title = 'Default title';
      $settings->hide_taxonomy_vocab = '';
      $settings->hide_content_entity = '';
      $settings->target_pages = '';
      $settings->tracking_info = 'Enter here tracking info from tracking service';
      $settings->position = 'header';
      $settings->weight = 0;
    }
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function trackingInjectGetDomainsInUse($domain) {
    if ($domain == 'tracking_inject_domain_not_set') {
      // drupal_set_message(t('Please set the domain for tracking.'), 'error');
      return;
    }
    return db_query('SELECT id FROM {tracking_inject} WHERE domain=:domain ORDER BY id ASC',
      array(':domain' => $domain))->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function trackingInjectCheckDateInRange($start_date, $end_date, $date_to_check) {
    if (is_null($start_date) && is_null($end_date) && is_null($date_to_check)) {
      // No date range found. Run it.
      return TRUE;
    }
    else {
      // Convert to timestamp.
      $start_ts = strtotime($start_date);
      $end_ts = strtotime($end_date);
      $user_ts = strtotime($date_to_check);

      // Check that user date is between start & end.
      return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));

    }
  }

}
